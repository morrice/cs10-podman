# cs10 podman

Very simple script to massage the CentOS Stream 10 daily build rootfs into a podman/docker compatible image

Looks like this:
```
$ ./cs10-podman.sh 
Downloading CentOS-Stream-Container-Base-10-20240410.0.x86_64.tar.xz
CentOS-Stream-Container-Minimal-10-20240410.0.x86_64.tar.xz to /tmp/tmp.XPblA3qQXR
extracting stuff
creating podman image
STEP 1/3: FROM scratch
STEP 2/3: ADD . /
--> de1c5824188
STEP 3/3: ENTRYPOINT /bin/bash
COMMIT cs10
--> 6fbbdde6074
Successfully tagged localhost/cs10:latest
6fbbdde60748efaf7ed0893a118cc25dd2818675d8070d5432e45d17aefd73b1
all done!
You can now enjoy brokenedge stuff with:
podman run -it cs10
[root@morricedesktop cs9-podman]# podman run -it cs10
[root@7585793ca2dc /]# 
[root@7585793ca2dc /]# cat /etc/redhat-release 
CentOS Stream release 10
[root@7585793ca2dc /]# dnf install vim
CentOS Stream 10 - BaseOS                                                                                                                                                            7.5 MB/s | 2.0 MB     00:00    
CentOS Stream 10 - AppStream                                                                                                                                                          34 MB/s | 6.4 MB     00:00    
Dependencies resolved.
=====================================================================================================================================================================================================================
 Package                                               Architecture                                  Version                                                  Repository                                        Size
=====================================================================================================================================================================================================================
Installing:
 vim-enhanced                                          x86_64                                        2:9.1.083-1.el10                                         appstream                                        1.9 M
Installing dependencies:
 vim-common                                            x86_64                                        2:9.1.083-1.el10                                         appstream                                        7.2 M
 vim-filesystem                                        noarch                                        2:9.1.083-1.el10                                         baseos                                            18 k
 which                                                 x86_64                                        2.21-41.el10                                             baseos                                            41 k
 xxd                                                   x86_64                                        2:9.1.083-1.el10                                         appstream                                         37 k

Transaction Summary
=====================================================================================================================================================================================================================
Install  5 Packages

Total download size: 9.2 M
Installed size: 41 M
Is this ok [y/N]:
```
